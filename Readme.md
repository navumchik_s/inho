#  INHO test task

For successful installation you need docker

1. Run `docker-compose up`
2. Connect to the container with CMD ` docker exec -it inho_application bash`
3. Run `./composer.phar install`
4. Run `vendor/bin/phalcon migration run`

Application located on `http://127.0.0.1:8077`

Examples of use:

1. Search by city name `http://127.0.0.1:8077/?search=Berlin`
2. Search by lat lon `http://127.0.0.1:8077/?search=53.8984147,27.5505842`
3. Get list of previous search cities `http://127.0.0.1:8077/history?limit=20&offset=3`
(by default limit = 1000, offset = 0) 

