<?php

use Phalcon\Db\Column as Column;
use Phalcon\Mvc\Model\Migration;
use Phalcon\Db\Index as Index;

class SearchMigration_100 extends Migration
{
    public function up()
    {
        $this->morphTable(
            'search_history',
            [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'size' => 10,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'first' => true,
                        ]
                    ),
                    new Column(
                        'keyword',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'notNull' => true,
                            'autoIncrement' => false,
                            'after' => 'id',
                        ]
                    ),
                    new Column(
                        'result',
                        [
                            'type' => Column::TYPE_TEXT,
                            'notNull' => false,
                            'autoIncrement' => false,
                            'after' => 'keyword',
                        ]
                    ),
                    new Column(
                        'createdAt',
                        [
                            'type' => Column::TYPE_TIMESTAMP,
                            'notNull' => false,
                            'autoIncrement' => false,
                            'after' => 'result',
                            'default' => 'CURRENT_TIMESTAMP',
                        ]
                    ),
                ],
                'indexes' => [
                    new Index(
                        'PRIMARY',
                        [
                            'id',
                        ]
                    )
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci',
                ],
            ]
        );
    }
}