<?php

use Phalcon\Mvc\Model;

class SearchHistory extends Model
{
    public $id;
    public $keyword;
    public $result;
    public $createdAt;
}
