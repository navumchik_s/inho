<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class IndexController extends Controller
{
    public function indexAction()
    {
        $this->view->disable();
        $response = new Response();
        $searchPhrase = $this->request->get('search');
        $response->setContent(json_encode($this->getWeather($searchPhrase)));
        return $response;
    }

    private function getWeather($searchPhrase): array
    {
        $latLonPattern = '/^([0-9]{1,2}\.?[0-9]*),([0-9]{1,2}\.?[0-9]*)$/';

        if (preg_match($latLonPattern, $searchPhrase, $matches)) {
            $requestParas = [
                'lat' => $matches[1],
                'lon' => $matches[2],
            ];
            $isSearchCity = false;
        } else {
            $requestParas = [
                'q' => $searchPhrase,
            ];
            $isSearchCity = true;
        }
        $requestParas['appid'] = $this->appConfig->weatherApi->key;
        $requestUrl = $this->appConfig->weatherApi->endpoint . '?' . http_build_query($requestParas);
        $result = json_decode(file_get_contents($requestUrl), true);

        (new SearchHistory([
            'keyword' => $searchPhrase,
            'result' => json_encode($result),
        ]))->save();

        $cityName = $result['name'] ?? ($isSearchCity ? $searchPhrase : null);
        if ($cityName) {
            /** @var Predis\Client $redis */
            $redis = $this->redis;
            $redis->lpush(HistoryController::LIST_KEY, $cityName);
        }

        return $result;
    }
}
