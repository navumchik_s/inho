<?php

use Phalcon\Mvc\Controller;

class HistoryController extends Controller
{
    const LIST_KEY = 'cityHistory';

    public function indexAction()
    {
        echo json_encode($this->getHistory());
    }

    private function getHistory(): array
    {
        /** @var Predis\Client $redis */
        $redis = $this->redis;
        return $redis->lrange(
            self::LIST_KEY,
            (int)$this->request->get('offset') ?: 0,
            (int)$this->request->get('limit') ?: 1000
        );
    }
}