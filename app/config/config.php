<?php
return new Phalcon\Config([
    'database' => [
        'adapter' => 'MySql',
        'host' => 'inho_db',
        'username' => 'root',
        'password' => 'root',
        'dbname' => 'inho',
    ],
    'redis' => [
        'host' => 'inho_redis',
        'port' => 6379,
        'auth' => false,
    ],
    'weatherApi' => [
        'key' => '04dfe4b5a6577eda4cfc194b537d5e53',
        'endpoint' => 'http://api.openweathermap.org/data/2.5/weather',
    ]
]);
