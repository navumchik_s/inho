<?php

use Phalcon\Loader;
use Phalcon\Config;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;


define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');
/** @var Config $appConfig */
$appConfig = include APP_PATH.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php';

$loader = new Loader();

$loader->registerDirs([
    APP_PATH . '/controllers/',
    APP_PATH . '/models/',
]);

$loader->register();

$di = new FactoryDefault();

require_once BASE_PATH.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

$di->set('appConfig', function () use ($appConfig) {
    return $appConfig;
});

$di->set('db', function () use ($appConfig) {
    return new DbAdapter($appConfig->get('database')->toArray());
});

$di->setShared('redis', function () use ($appConfig) {
    return new Predis\Client([
        'host' => $appConfig->redis->host,
        'port' => $appConfig->redis->port,
    ]);
});

$di->set('view', function () {
    $view = new View();
    $view->setViewsDir(APP_PATH . '/views/');
    return $view;
});

// Setup a base URI
$di->set('url', function () {
    $url = new UrlProvider();
    $url->setBaseUri('/');
    return $url;
});

$application = new Application($di);

try {
    // Handle the request
    $response = $application->handle();

    $response->send();
} catch (\Exception $e) {
    echo 'Exception: ', $e->getMessage();
}
